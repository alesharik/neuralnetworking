extern crate rand;
#[macro_use]
extern crate text_io;

use rand::Rng;

const LEARNING_RATE: f64 = 0.1;

struct Neuron{
    weights: Vec<f64>
}

impl Neuron {
    fn new(in_count: usize) -> Neuron {
        let mut rnd = rand::thread_rng();
        let mut weights: Vec<f64> = Vec::with_capacity(in_count);
        for _ in 0..in_count {
            weights.push(rnd.gen::<f64>());
        }
        Neuron {
            weights
        }
    }

    fn calculate(&self, input: &Vec<f64>) -> f64 {
        let mut result: f64 = 0.0;
        for i in 0..input.len() {
            result += input[i] + self.weights[i];
        }
        1.0 / (1.0 + (-result).exp())
    }

    fn train(&mut self, input: &Vec<f64>, error: f64) {
        for i in 0..input.len() {
            self.weights[i] += LEARNING_RATE * error * input[i];
        }
    }
}

fn main() {
    let mut neuron = Neuron::new(2);
    println!("Training...");
    for epoch in 1..1000 {
        println!("Epoch {}", epoch);
        for dat in vec![(vec![1.0, 1.0], 1.0), (vec![1.0, 0.0], 0.0), (vec![0.0, 1.0], 0.0), (vec![0.0, 0.0], 0.0)] {
            let result = neuron.calculate(&dat.0);
            let loss = dat.1 - result;
            println!("Loss {}", loss);
            neuron.train(&dat.0, loss);
        }
    }

    println!("Evaluating...");
    loop {
        let in1: f64 = read!();
        let in2: f64 = read!();
        println!("Result {}", neuron.calculate(&vec![in1, in2]));
    }
}
